import React from 'react';

class CandleCount extends React.Component {


    constructor(props) {
        super(props);
    }

    renderCandles = (numberOfCandles) => {
        /*
        const dots = Array(...Array(parseInt(numberOfDots, 10)));

        return (
            <View style={styles.dotContainer}>
                {dots.map((dot, index) =>
                    <View style={[styles.dot, parseInt(selectedDot, 10) === (index + 1) ? { backgroundColor: Styles.colours.crusta } : null]} key={index}/>)}
            </View>
        );
        */
        const candleList = Array(...Array(parseInt(numberOfCandles)));
        console.log(candleList);
        
        return (
            <div>
                {candleList.map((candle, index) =>
                    <img style={styles.candle} src={require('../images/lit-candle-icon.png')} key={index}/>
                )}
            </div>
            
        )
    }
    

    render() {
        return(
            <div>
                <div style={styles.container}>
                    <h1 style={styles.textCount}>{this.props.candleCount}</h1>
                    <div style={styles.wrapper}>
                        <p style={styles.text}>candles</p>
                        <div style={styles.line}></div>
                    </div>
                </div>
                <div style={styles.candlesList}>
                    {this.renderCandles(this.props.candleCount)}
                </div>
            </div>
        )
    }
}

const styles = {
    container: {
        padding: '16px'
    },
    textCount: {
        color: '#8293a8',
        fontWeight: 600,
        fontSize: '60px',
        lineHeight: 1,
        textAlign: 'center',
        margin: 0
    },
    wrapper: {
        position: 'relative'
    },
    text: {
        color: '#8293a8',
        fontSize: '18px',
        lineHeight: 2,
        textAlign: 'center',
        margin: 0
    },
    candlesList: {
        textAlign: 'center',
        maxWidth: '500px',
        margin: '16px auto'
    },
    candle: {
        width: '28px',
        height: '28px',
        marginBottom: '8px'
    },
    line: {
        borderTop: '2px solid #ACCC64',
        margin: '8px auto 0 auto',
        width: '40px'
    }
}

export default CandleCount;