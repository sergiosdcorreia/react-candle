import React from 'react';

import ShareBar from './ShareBar';
import CandleCount from './CandleCount';
import candle_bg from '../images/candle_bg.gif';
import candle_bg_unlit from '../images/candle_bg_unlit_sm.png';
import WebCandleStart from '../images/WebCandleStart20fpsNewbg.gif';
import WebCandleLoop from '../images/WebCandleLoop15fps120wNewbg.gif';
import Swipe from './Swipe';

class Header extends React.Component {
    constructor(props) {
        super(props);

        let sessionLit = false;

        let secondsFromLit = localStorage.getItem('isCandleLit');
        let now = Math.round(new Date() / 1000);

        if (secondsFromLit && (now - secondsFromLit) <= 86400){
            sessionLit = true;
        } else {
            localStorage.removeItem('isCandleLit');
        }

        this.state = {
            backgroundImage: undefined,
            isLit: sessionLit,
            imageSource: sessionLit ? WebCandleLoop : undefined,
            inFront: true,
            top: 145,
            candleCount: 20
        };
    }
    
    lightCandle = () => 

        (localStorage.setItem('isCandleLit', Math.round(new Date() / 1000)),
            
            this.setState({
                imageSource: WebCandleStart,
                isLit: true,
                candleCount: this.state.candleCount + 1
            }),

            setTimeout( () => 
                this.setState({
                    backgroundImage: WebCandleLoop,
                    inFront: false,
                    top: 208
                })
            , 7500)
        );


    render() {
        return (
            <div>
                <div style={styles.ImageBg}>
                    <div style={styles.ImagePlaceholder}>
                        <img style={{position: 'absolute', left: '0px', top: '145px', width: '120px', zIndex: 1}} src={ candle_bg_unlit }/>
                        <img style={{position: 'absolute', left: '0px', top: this.state.top + 'px', width: '120px', zIndex: !this.state.inFront ? 10 : 2}} src={ this.state.backgroundImage }/>
                        <img style={{position: 'absolute', left: '0px', top: '208px', width: '120px', zIndex: this.state.inFront ? 10 : 2}} src={ this.state.imageSource }/>
                    </div>
                </div>
                { this.state.isLit ? <ShareBar /> : <Swipe lightCandle={this.lightCandle}/>}
                <div>
                    <CandleCount candleCount={this.state.candleCount}/>
                </div>
            </div>
        )
    }
}

const styles = {
    ImageBg: {
        backgroundImage: `url(${candle_bg})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'repeat-x',
        textAlign: 'center',
        height: '343px'
    },
    ImagePlaceholder: {
        width: '120px',
        margin: '0 auto',
        position: 'relative'
    }
}

export default Header;