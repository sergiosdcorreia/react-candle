import React from 'react';

import IconFacebook from '../images/facebook';
import IconPinterest from '../images/pinterest';
import IconTwitter from '../images/twitter';
import IconMessenger from '../images/messenger';


class ShareBar extends React.Component {
    render() {
        return(
            <div style={styles.shareBar}>
                <div style={styles.shareIconsContainer}>
                    <a href="#" style={styles.icon}><IconFacebook /></a>
                    <a href="#" style={styles.icon}><IconPinterest /></a>
                    <a href="#" style={styles.icon}><IconTwitter /></a>
                    <a href="#" style={styles.icon}><IconMessenger /></a>
                </div>
            </div>
        )
    }
}

const styles = {
    shareBar: {
        backgroundColor: '#ACCC64',
    },
    shareIconsContainer: {
        display: 'flex',
        width: '256px',
        margin: '0 auto',
        alignItems: 'center',
        height: '72px'
    },
    icon: {
        paddingLeft: '16px',
        paddingRight: '16px'
    }
}

export default ShareBar;