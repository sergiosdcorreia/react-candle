import React from 'react';

const animationWidth = 480;
const swiperMargin= 15;
const dotSize = 30;
const dotShift = 8;
const sliderWidth = (animationWidth - (swiperMargin*2)) - dotSize - (dotShift*2);

class Swipe extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            left: dotShift,
            originalOffset: 0,
            isDragged: false,
            containerOpacity: 1
        };
    }

    // On click, calculate the starting point of the drag
    handleStart(downEvent, type) {
        let clientX;
        
        if (type === "mouse") {
            clientX = downEvent.clientX
        } else if ( type === "touch" && downEvent.touches) {
            clientX = downEvent.touches[0].clientX
        }

        this.setState({
            isDragged: true,
            originalOffset: clientX,
        });
    }


    // Change position on click and drag
    handleMove(moveEvent, type) {
        let clientX;
        
        if (type === "mouse") {
            clientX = moveEvent.clientX
        } else if ( type === "touch" && moveEvent.touches) {
            clientX = moveEvent.touches[0].clientX
        }

        if (this.state.isDragged){
            const deltaX = clientX - this.state.originalOffset;

            if (deltaX > dotShift && deltaX <= sliderWidth && this.state.isDragged){

                let opacity = 1;
                const position = Math.abs(deltaX) / (sliderWidth);
        
                // it doesn't change the opacity until the slider reaches 1/3 of its max
                // above 33%, opacity is linearly decreased until it reaches 66%
                if (position >= 0.33) {
                    opacity = 1 - (( position - 0.33 ) * 2.5 );
                    this.setState({
                        containerOpacity: opacity,
                        left: deltaX
                    })

                    if (position >= 0.66) {
                        this.props.lightCandle();
                        this.props.candleCount();
                    }

                } else {
                    this.setState({
                        left: deltaX
                    })                    
                }
            }
        }
    }
    
    handleMouseUp() {
        this.setState({
            isDragged: false,
            left: 16,
            originalOffset: 0,
            containerOpacity: 1
        });
    }


    updateStyleBasedOnDeltaX(deltaX) {
        const position = Math.abs(deltaX) / (sliderWidth);
        let opacity = 1;

        // it doesn't change the opacity until the slider reaches 1/3 of its max
        // above 33%, opacity is linearly decreased until it reaches 66%
        if (position >= 0.33) {
            opacity = 1 - (( position - 0.33 * 2 ));
            
            this.setState({
                containerOpacity: opacity
            })
        }
    }

    render() {
        return (
            <div style={{opacity: this.state.containerOpacity}}>
                <div style={styles.swiper}>
                    <div style={styles.slider}
                        onMouseMove={mouseMoveEvent => this.handleMove(mouseMoveEvent, "mouse")}
                        onTouchMove={touchMoveEvent => this.handleMove(touchMoveEvent, "touch")}
                        onMouseUp={() => this.handleMouseUp()}
                        onTouchEnd={() => this.handleMouseUp()}
                    >
                        <h1 style={styles.text}>Swipe to light</h1>
                        <div style={{position: 'absolute', top: '6px', left: this.state.left + 'px'}}>
                            <div style={styles.dot}
                                onMouseDown={mouseDownEvent => this.handleStart(mouseDownEvent, "mouse")}
                                onTouchStart={touchStartEvent => this.handleStart(touchStartEvent, "touch")}
                            />
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

const styles = {
    swiper: {
        backgroundColor: '#e5e5e5',
        alignItems: 'center',
        margin: '0 auto',
        padding: '6px',
    },
    slider: {
        height: '40px',
        backgroundColor: '#fff',
        borderColor: 'orange',
        borderStyle: 'solid',
        borderWidth: '2px',
        borderRadius: '40px',
        position: 'relative',
        maxWidth: animationWidth,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    dot: {
        height: dotSize,
        width: dotSize,
        backgroundColor: '#fff',
        borderColor: 'orange',
        borderWidth: '4px',
        borderStyle: 'solid',
        borderRadius: dotSize/2,
        shadowOpacity: '0.3px',
        position: 'relative',
        bottom: '3px'
    },
    text: {
        position: 'relative',
        left: '50px',
        bottom: '5px',
        color: '#8293A8',
        fontSize: '16px',
        width: '200px'
    }
}

export default Swipe;